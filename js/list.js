var llistaActual;
function loadLists() {
    var endpoint_lists = 'https://apidoit.herokuapp.com/todoList';

    fetch(endpoint_lists)
        .then(response => response.json())
        .then(data => renderLists(data)).catch(function(){
        alert("No es poden obtenir les llistes, torna a provar-ho");
    });
}

function renderLists(llistes) {
    for (i = 0; i < llistes.length; i++) {
        renderListTitles(llistes[i]);
    }
}

function renderListTitles(llista) {
    var t = document.createTextNode(llista.name);
    var li = document.createElement("li");
    li.classList.add('list-group-item');
    li.appendChild(t);

    document.getElementById("llistes").appendChild(li);

    li.addEventListener('click', function () {
        llistaActual = llista
        loadItems(llista);
        document.getElementById("listName").innerHTML = llista.name ;
        closeNav();
    });
}

function loadItems(llista) {
    var endpoint_list = 'https://apidoit.herokuapp.com/todoList/'+llista.id+'/todoItem';

    fetch(endpoint_list)
        .then(response => response.json())
        .then(data => renderItems(data)).catch(function() {
        prepareBodyForSelectList();
        alert("No s'ha pogut obtenir les tasques de la llista '"+llista.name+"' amb id:"+llista.id+"\n"+endpoint_list)
    });
}


function updateTaskStatus(element){
    element.fet = !element.fet
    var endpoint_list = 'https://apidoit.herokuapp.com/todoList/'+llistaActual.id+'/todoItem';

    delete element.llista;

    console.log(JSON.stringify(element));
    fetch(endpoint_list, {
        method: 'PUT',
        body: JSON.stringify(element),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(response => loadItems(llistaActual))
        .catch(error => console.error('Error:', error));
}

function addTask(element){

    var endpoint_list = 'https://apidoit.herokuapp.com/todoList/'+llistaActual.id+'/todoItem';

    console.log(JSON.stringify(element));
    fetch(endpoint_list, {
        method: 'POST',
        body: JSON.stringify(element),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(response => loadItems(llistaActual))
        .catch(error => console.error('Error:', error));


}

function renderItems(items) {
    document.getElementById("containerLlista").style.display = "block";
    document.getElementById("containerSplash").style.display = "none";

    document.getElementById("llista").innerHTML = "" ;

    for (i = 0; i < items.length; i++) {
        renderItem(items[i]);
    }
}

function renderItem(element) {
    var t = document.createTextNode(element.description);
    var li = document.createElement("li");
    li.classList.add('list-group-item');
    li.appendChild(t);

    document.getElementById("llista").appendChild(li);
    if (element.stat) {
        li.classList.add('task-done')
    }

    li.addEventListener('click', function () {
        updateTaskStatus(element)
        //this.classList.toggle('task-done');
    });

}

function prepareBodyForSelectList(){
    document.getElementById("containerLlista").style.display = "none";
    document.getElementById("containerSplash").style.display = "block";
}

//run when the DOM is ready
$(function () {
    loadLists();
    prepareBodyForSelectList();
    inputItemsManager();
});

/**
 *  Manage the input for add new Items
 */
function inputItemsManager() {
    const element = document.querySelector('form');
    element.addEventListener('submit', event => {
        event.preventDefault(); // it prevents the default submint action

        // get the text from the form input 'taskInput'
        var inputValue = document.getElementById("taskInput").value;

        //alert or render
        (inputValue === '') ? alert("Escriu una tasca") : addTask(new Element(inputValue, false))

        // delete the text from input 'taskInput'
        document.getElementById("taskInput").value = "";
    });
}

class Element {
    constructor(description, stat) {
        this.description = description;
        this.stat = stat;
    }
}