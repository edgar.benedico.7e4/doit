# DOIT

## INTRODUCCIÓN
Esta página web cumple la funcion de una todolist, con ella serás capaz de organizar tu día a día con una serie de tareas que podrás administrar tú mismo.

## FUNCIONES DE LA PÁGINA
Desde la página web puedes administrar una todolist propia dónde podrás crear, modificar y borrar tanto tareas como listas de tareas.

## DE DÓNDE Y HACIA DÓNDE
Todos los datos de la todolist se guardan en una base de datos gestionada a través de una API Rest, la página tan solo manda peticiones para cargar todo el contenido y modificar el que sea necesario.

## DOCUMENTACIÓN ADICIONAL
https://docs.google.com/document/d/1Ro_nfCBi5_3c-7UudnGzNct2PwlXNWAFBxyOpHv28i4/edit
